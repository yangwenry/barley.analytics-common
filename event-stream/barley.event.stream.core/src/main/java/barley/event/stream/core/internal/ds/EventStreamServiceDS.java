/*
 * Copyright (c) 2015, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package barley.event.stream.core.internal.ds;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import barley.core.utils.BarleyUtils;
import barley.core.utils.ConfigurationContextService;
import barley.event.stream.core.EventStreamListener;
import barley.event.stream.core.exception.EventStreamConfigurationException;
import barley.event.stream.core.internal.CarbonEventStreamService;
import barley.event.stream.core.internal.EventStreamRuntime;
import barley.event.stream.core.internal.util.EventStreamConstants;
import barley.event.stream.core.internal.util.helper.EventStreamConfigurationFileSystemInvoker;

/**
 * @scr.component name="eventStreamService.component" immediate="true"
 * @scr.reference name="config.context.service"
 * interface="org.wso2.carbon.utils.ConfigurationContextService" cardinality="0..1" policy="dynamic"
 * bind="setConfigurationContextService" unbind="unsetConfigurationContextService"
 * @scr.reference name="eventStreamListener.service"
 * interface="org.wso2.carbon.event.stream.core.EventStreamListener" cardinality="0..n" policy="dynamic"
 * bind="setEventStreamListener" unbind="unsetEventStreamListener"
 */
public class EventStreamServiceDS {
    private static final Log log = LogFactory.getLog(EventStreamServiceDS.class);

    public void activate() {
        try {
            EventStreamServiceValueHolder.registerEventStreamRuntime(new EventStreamRuntime());
            CarbonEventStreamService carbonEventStreamService = new CarbonEventStreamService();
            EventStreamServiceValueHolder.setCarbonEventStreamService(carbonEventStreamService);
            // (임시주석)
            //context.getBundleContext().registerService(EventStreamService.class.getName(), carbonEventStreamService, null);
            
            // (추가) 시작시 강제 stream deploy 수행
            deployDefaultEventStreamConfiguration();
            
            if (log.isDebugEnabled()) {
                log.debug("Successfully deployed EventStreamService");
            }
        } catch (Throwable e) {
            log.error("Could not create EventStreamService : " + e.getMessage(), e);
        }
    }

    private void deployDefaultEventStreamConfiguration() {
    	String mappingResourcePath = BarleyUtils.getCarbonConfigDirPath() + File.separator + EventStreamConstants.EVENT_STREAMS;
//    	String throttleRequestStreamFile = mappingResourcePath + File.separator + EventStreamConstants.THROTTLE_REQUEST_STREAM_FILE; 
//    	String statisticsRequestStreamFile = mappingResourcePath + File.separator + EventStreamConstants.STATISTICS_REQUEST_STREAM_FILE;
    	try {
    		File mappingResourceDir = new File(mappingResourcePath);
    		if(mappingResourceDir.exists()) {
    			for(File configFile : mappingResourceDir.listFiles()) {
    				EventStreamConfigurationFileSystemInvoker.deploy(configFile);
    			}
    		}
//    		EventStreamConfigurationFileSystemInvoker.deploy(throttleRequestStreamFile);
//			EventStreamConfigurationFileSystemInvoker.deploy(statisticsRequestStreamFile);
		} catch (EventStreamConfigurationException e) {
			log.error(e.getMessage(), e);
		}
	}

	protected void setConfigurationContextService(ConfigurationContextService configurationContextService) {
        EventStreamServiceValueHolder.registerConfigurationContextService(configurationContextService);

        if (EventStreamServiceValueHolder.getCarbonEventStreamService() != null) {
            EventStreamServiceValueHolder.getCarbonEventStreamService().addPendingStreams();
        }
    }

    protected void unsetConfigurationContextService(ConfigurationContextService configurationContextService) {
        EventStreamServiceValueHolder.registerConfigurationContextService(null);
    }

    protected void setEventStreamListener(EventStreamListener eventStreamListener) {
        EventStreamServiceValueHolder.registerEventStreamListener(eventStreamListener);
    }

    protected void unsetEventStreamListener(EventStreamListener eventStreamListener) {
        EventStreamServiceValueHolder.unregisterEventStreamListener(eventStreamListener);
    }
}
