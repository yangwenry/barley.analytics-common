/*
 * Copyright (c) 2015, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package barley.event.receiver.core.internal.ds;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import org.osgi.service.component.ComponentContext;

import barley.core.configuration.ServerConfiguration;
import barley.core.utils.BarleyUtils;
import barley.core.utils.ConfigurationContextService;
import barley.event.input.adapter.core.InputEventAdapterFactory;
import barley.event.input.adapter.core.InputEventAdapterService;
import barley.event.processor.manager.core.EventManagementService;
import barley.event.receiver.core.EventReceiverService;
import barley.event.receiver.core.config.EventReceiverConstants;
import barley.event.receiver.core.exception.EventReceiverConfigurationException;
import barley.event.receiver.core.internal.CarbonEventReceiverManagementService;
import barley.event.receiver.core.internal.CarbonEventReceiverService;
import barley.event.receiver.core.internal.util.EventReceiverConfigurationBuilder;
import barley.event.receiver.core.internal.util.helper.EventReceiverConfigurationFileSystemInvoker;
import barley.event.stream.core.EventStreamService;
import barley.event.stream.core.internal.util.helper.EventStreamConfigurationFileSystemInvoker;
import barley.registry.api.RegistryException;
import barley.registry.api.RegistryService;
import barley.user.core.service.RealmService;

/**
 * @scr.component name="eventReceiverService.component" immediate="true"
 * @scr.reference name="inputEventAdapter.service"
 * interface="org.wso2.carbon.event.input.adapter.core.InputEventAdapterService" cardinality="1..1"
 * policy="dynamic" bind="setInputEventAdapterService" unbind="unsetInputEventAdapterService"
 * @scr.reference name="input.event.adapter.tracker.service"
 * interface="org.wso2.carbon.event.input.adapter.core.InputEventAdapterFactory" cardinality="0..n"
 * policy="dynamic" bind="setEventAdapterType" unbind="unSetEventAdapterType"
 * @scr.reference name="eventManagement.service"
 * interface="org.wso2.carbon.event.processor.manager.core.EventManagementService" cardinality="1..1"
 * policy="dynamic" bind="setEventManagementService" unbind="unsetEventManagementService"
 * @scr.reference name="registry.service"
 * interface="org.wso2.carbon.registry.core.service.RegistryService"
 * cardinality="1..1" policy="dynamic" bind="setRegistryService" unbind="unsetRegistryService"
 * @scr.reference name="eventStreamManager.service"
 * interface="org.wso2.carbon.event.stream.core.EventStreamService" cardinality="1..1"
 * policy="dynamic" bind="setEventStreamService" unbind="unsetEventStreamService"
 * @scr.reference name="config.context.service"
 * interface="org.wso2.carbon.utils.ConfigurationContextService" cardinality="0..1" policy="dynamic"
 * bind="setConfigurationContextService" unbind="unsetConfigurationContextService"
 * @scr.reference name="user.realmservice.default" interface="org.wso2.carbon.user.core.service.RealmService"
 * cardinality="1..1" policy="dynamic" bind="setRealmService"  unbind="unsetRealmService"
 */
public class EventReceiverServiceDS {
    private static final Log log = LogFactory.getLog(EventReceiverServiceDS.class);

    public void activate() {
        try {

            checkIsStatsEnabled();
            CarbonEventReceiverService carbonEventReceiverService = new CarbonEventReceiverService();
            EventReceiverServiceValueHolder.registerEventReceiverService(carbonEventReceiverService);

            CarbonEventReceiverManagementService carbonEventReceiverManagementService = new CarbonEventReceiverManagementService();
            EventReceiverServiceValueHolder.getEventManagementService().subscribe(carbonEventReceiverManagementService);

            EventReceiverServiceValueHolder.registerReceiverManagementService(carbonEventReceiverManagementService);

            // (임시주석)
            //context.getBundleContext().registerService(EventReceiverService.class.getName(), carbonEventReceiverService, null);
            if (log.isDebugEnabled()) {
                log.debug("Successfully deployed EventReceiverService.");
            }

            activateInactiveEventReceiverConfigurations(carbonEventReceiverService);
            /* (임시주석)
            context.getBundleContext().registerService(EventStreamListener.class.getName(),
                    new EventStreamListenerImpl(), null);
            ArrayList<CarbonTomcatValve> valves = new ArrayList<CarbonTomcatValve>();
            valves.add(new TenantLazyLoaderValve());
            TomcatValveContainer.addValves(valves);
            */
            
            // (추가) 2018.09.14 - 시작시 강제 config deploy 수행
            deployDefaultEventReceiverConfiguration();
            
        } catch (Throwable e) {
            log.error("Could not create EventReceiverService or EventReceiver : " + e.getMessage(), e);
        }
    }

    private void checkIsStatsEnabled() {
        ServerConfiguration config = ServerConfiguration.getInstance();
        String confStatisticsReporterDisabled = config.getFirstProperty("StatisticsReporterDisabled");
        if (!"".equals(confStatisticsReporterDisabled)) {
            boolean disabled = Boolean.valueOf(confStatisticsReporterDisabled);
            if (disabled) {
                return;
            }
        }
        EventReceiverServiceValueHolder.setGlobalStatisticsEnabled(true);
    }

    private void activateInactiveEventReceiverConfigurations(CarbonEventReceiverService carbonEventReceiverService) {
        Set<String> inputEventAdapterTypes = EventReceiverServiceValueHolder.getInputEventAdapterTypes();
        inputEventAdapterTypes.addAll(EventReceiverServiceValueHolder.getInputEventAdapterService().getInputEventAdapterTypes());
        for (String type : inputEventAdapterTypes) {
            try {
            	carbonEventReceiverService.activateInactiveEventReceiverConfigurationsForAdapter(type);
            } catch (EventReceiverConfigurationException e) {
                log.error(e.getMessage(), e);
            }
        }
    }
    
    // (추가)
    private void deployDefaultEventReceiverConfiguration() {
    	String mappingResourcePath = BarleyUtils.getCarbonConfigDirPath() + File.separator + EventReceiverConstants.ER_CONFIG_DIRECTORY; 
//				+ File.separator + EventReceiverConstants.THROTTLE_EVENT_RECEIVER_FILE;
    	try {
//        	String eventReceiverConfigXml = getExistingEventReceiverConfigXml(mappingResourcePath);
//			carbonEventReceiverService.deployEventReceiverConfiguration(eventReceiverConfigXml);
    		
    		File mappingResourceDir = new File(mappingResourcePath);
    		if(mappingResourceDir.exists()) {
    			for(File configFile : mappingResourceDir.listFiles()) {
    				EventReceiverConfigurationFileSystemInvoker.deploy(configFile.getAbsolutePath());
    			}
    		}
//        	EventReceiverConfigurationFileSystemInvoker.deploy(mappingResourcePath);
        } catch (EventReceiverConfigurationException e) {
            log.error(e.getMessage(), e);
        }
	}

    public void setInputEventAdapterService(InputEventAdapterService inputEventAdapterService) {
        EventReceiverServiceValueHolder.registerInputEventAdapterService(inputEventAdapterService);
    }
    
    public void unsetInputEventAdapterService(
            InputEventAdapterService inputEventAdapterService) {
        EventReceiverServiceValueHolder.getInputEventAdapterTypes().clear();
        EventReceiverServiceValueHolder.registerInputEventAdapterService(null);
    }

    protected void setRegistryService(RegistryService registryService) throws RegistryException {
        EventReceiverServiceValueHolder.registerRegistryService(registryService);
    }

    protected void unsetRegistryService(RegistryService registryService) {
        EventReceiverServiceValueHolder.registerRegistryService(null);
    }

    public void setEventStreamService(EventStreamService eventStreamService) {
        EventReceiverServiceValueHolder.registerEventStreamService(eventStreamService);
    }

    public void unsetEventStreamService(EventStreamService eventStreamService) {
        EventReceiverServiceValueHolder.registerEventStreamService(null);
    }

    protected void setConfigurationContextService(
            ConfigurationContextService configurationContextService) {
        EventReceiverServiceValueHolder.setConfigurationContextService(configurationContextService);
    }

    protected void unsetConfigurationContextService(
            ConfigurationContextService configurationContextService) {
        EventReceiverServiceValueHolder.setConfigurationContextService(null);

    }

    public void setEventAdapterType(InputEventAdapterFactory inputEventAdapterFactory) {
        EventReceiverServiceValueHolder.addInputEventAdapterType(inputEventAdapterFactory.getType());
        if (EventReceiverServiceValueHolder.getCarbonEventReceiverService() != null) {
            try {
                EventReceiverServiceValueHolder.getCarbonEventReceiverService().activateInactiveEventReceiverConfigurationsForAdapter(inputEventAdapterFactory.getType());
            } catch (EventReceiverConfigurationException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public void unSetEventAdapterType(InputEventAdapterFactory inputEventAdapterFactory) {
        EventReceiverServiceValueHolder.removeInputEventAdapterType(inputEventAdapterFactory.getType());
        if (EventReceiverServiceValueHolder.getCarbonEventReceiverService() != null) {
            try {
                EventReceiverServiceValueHolder.getCarbonEventReceiverService().deactivateActiveEventReceiverConfigurationsForAdapter(inputEventAdapterFactory.getType());
            } catch (EventReceiverConfigurationException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public void setEventManagementService(EventManagementService eventManagementService) {
        EventReceiverServiceValueHolder.registerEventManagementService(eventManagementService);

    }

    public void unsetEventManagementService(EventManagementService eventManagementService) {
        EventReceiverServiceValueHolder.registerEventManagementService(null);
        eventManagementService.unsubscribe(EventReceiverServiceValueHolder.getCarbonEventReceiverManagementService());

    }

    protected void setRealmService(RealmService realmService) {
        EventReceiverServiceValueHolder.setRealmService(realmService);
    }

    protected void unsetRealmService(RealmService realmService) {
    }
    
    // (추가)
    /*private static String getExistingEventReceiverConfigXml(String filePath) throws IOException {
        File file = new File(filePath);
        if (file.exists()) {
            byte[] encoded = Files.readAllBytes(Paths.get(filePath));
            return new String(encoded);
        } else {
            return null;
        }
    }*/
}
