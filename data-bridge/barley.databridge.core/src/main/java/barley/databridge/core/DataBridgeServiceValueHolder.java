package barley.databridge.core;

import barley.core.utils.ConfigurationContextService;
import barley.databridge.core.definitionstore.AbstractStreamDefinitionStore;
import barley.user.core.service.RealmService;

public class DataBridgeServiceValueHolder {

    private static RealmService realmService;
    private static AbstractStreamDefinitionStore streamDefinitionStore;
    private static ConfigurationContextService configurationContextService;

    public static void setRealmService(RealmService realmService) {
        DataBridgeServiceValueHolder.realmService = realmService;
    }

    public static RealmService getRealmService() {
        return realmService;
    }

    public static void setStreamDefinitionStore(AbstractStreamDefinitionStore streamDefinitionStore) {
        DataBridgeServiceValueHolder.streamDefinitionStore = streamDefinitionStore;
    }

    public static AbstractStreamDefinitionStore getStreamDefinitionStore() {
        return streamDefinitionStore;
    }

    public static void setConfigurationContextService(ConfigurationContextService configurationContextService) {
        DataBridgeServiceValueHolder.configurationContextService = configurationContextService;
    }

    public static ConfigurationContextService getConfigurationContextService() {
        return configurationContextService;
    }
}
