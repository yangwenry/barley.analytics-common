package barley.databridge.agent.binary;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

import barley.core.MultitenantConstants;
import barley.core.ServerConstants;
import barley.core.context.PrivilegedBarleyContext;
import barley.core.internal.BarleyContextDataHolder;
import barley.core.internal.OSGiDataHolder;
import barley.core.utils.BarleyUtils;
import barley.databridge.commons.Credentials;
import barley.databridge.commons.Event;
import barley.databridge.commons.StreamDefinition;
import barley.databridge.core.AgentCallback;
import barley.databridge.core.DataBridgeReceiverService;
import barley.databridge.core.DataBridgeServiceValueHolder;
import barley.databridge.core.DataBridgeSubscriberService;
import barley.databridge.core.definitionstore.AbstractStreamDefinitionStore;
import barley.databridge.core.definitionstore.InMemoryStreamDefinitionStore;
import barley.databridge.core.internal.DataBridgeDS;
import barley.databridge.receiver.binary.internal.BinaryDataReceiverServiceComponent;
import barley.event.input.adapter.core.InputEventAdapterFactory;
import barley.event.input.adapter.core.InputEventAdapterService;
import barley.event.input.adapter.core.internal.CarbonInputEventAdapterService;
import barley.event.input.adapter.core.internal.ds.InputEventAdapterServiceDS;
import barley.event.input.adapter.core.internal.ds.InputEventAdapterServiceValueHolder;
import barley.event.input.adapter.wso2event.WSO2EventEventAdapterFactory;
import barley.event.input.adapter.wso2event.internal.ds.WSO2EventAdapterServiceDS;
import barley.event.output.adapter.core.OutputEventAdapterFactory;
import barley.event.output.adapter.core.OutputEventAdapterService;
import barley.event.output.adapter.core.internal.ds.OutputEventAdapterServiceDS;
import barley.event.output.adapter.core.internal.ds.OutputEventAdapterServiceValueHolder;
import barley.event.output.adapter.rdbms.RDBMSEventAdapterFactory;
import barley.event.output.adapter.rdbms.internal.ds.RDBMSEventAdapterServiceDS;
import barley.event.processor.manager.core.EventManagementService;
import barley.event.processor.manager.core.internal.ds.EventManagementServiceDS;
import barley.event.processor.manager.core.internal.ds.EventManagementServiceValueHolder;
import barley.event.publisher.core.internal.ds.EventPublisherServiceDS;
import barley.event.publisher.core.internal.ds.EventPublisherServiceValueHolder;
import barley.event.receiver.core.internal.ds.EventReceiverServiceDS;
import barley.event.receiver.core.internal.ds.EventReceiverServiceValueHolder;
import barley.event.stream.core.EventStreamService;
import barley.event.stream.core.internal.ds.EventStreamServiceDS;
import barley.event.stream.core.internal.ds.EventStreamServiceValueHolder;
import barley.identity.authentication.AuthenticationServiceImpl;
import barley.identity.authentication.SharedKeyAccessServiceImpl;
import barley.ndatasource.common.DataSourceException;
import barley.ndatasource.core.DataSourceManager;
import barley.ndatasource.core.DataSourceService;
import barley.registry.core.config.RegistryContext;
import barley.registry.core.jdbc.realm.InMemoryRealmService;
import barley.registry.core.service.RegistryService;
import barley.user.core.service.RealmService;
import junit.framework.TestCase;

public class BinaryTestServer {
	Logger log = Logger.getLogger(BinaryTestServer.class);
	
	private DataBridgeDS dataBridgeDS = null;
	private BinaryDataReceiverServiceComponent receiverComp = null;
	
	AtomicInteger numberOfEventsReceived;
	
	public void setUp(RealmService realmService) throws Exception {
		initBinaryReceiver(realmService);
	}

	private void initBinaryReceiver(RealmService realmService) {
		// 인메모리 스트림으로 테스트
		dataBridgeDS = new DataBridgeDS();
		AbstractStreamDefinitionStore streamDefinitionStore = new InMemoryStreamDefinitionStore();
		dataBridgeDS.setEventStreamStoreService(streamDefinitionStore);
		dataBridgeDS.setAuthenticationService(new AuthenticationServiceImpl(new SharedKeyAccessServiceImpl("admin"), realmService));
		dataBridgeDS.activate();
		
		DataBridgeReceiverService recieverService = dataBridgeDS.getDataBridgeReceiverService();
		DataBridgeSubscriberService subscriber = dataBridgeDS.getDataBridgeSubscriberService();
		
		receiverComp = new BinaryDataReceiverServiceComponent();
		receiverComp.setDataBridgeReceiverService(recieverService);
		
		EventManagementServiceDS eventManagementServiceDS = new EventManagementServiceDS(); 
		eventManagementServiceDS.activate();
		
		EventStreamServiceDS eventStreamServiceDS = new EventStreamServiceDS();
		eventStreamServiceDS.activate();
		
		activateEventReceiverService(subscriber);
		activateEventPublisherService();
		
		// receiver 시작 
		EventReceiverServiceValueHolder.getCarbonEventReceiverService().start();
	}
	
	private void activateEventReceiverService(DataBridgeSubscriberService subscriber) {
		// wso2는 초기화 하지 않는다. wso2Adapterfactory를 직접 생성하자.
		// 원래 input-event-adapters.xml에서 읽어와서 factory를 생성한다. 파일을 일단 제거했는데 필요하면 넣어도 된다. 
		WSO2EventAdapterServiceDS wso2EventAdapterServiceDS = new WSO2EventAdapterServiceDS();
		wso2EventAdapterServiceDS.setDataBridgeSubscriberService(subscriber);
		InputEventAdapterFactory wso2EventEventAdapterFactory = new WSO2EventEventAdapterFactory();
		
		InputEventAdapterServiceDS inputEventAdapterServiceDS = new InputEventAdapterServiceDS();
		inputEventAdapterServiceDS.setEventAdapterType(wso2EventEventAdapterFactory);
		inputEventAdapterServiceDS.activate();
		
		InputEventAdapterService inputEventAdapterService = InputEventAdapterServiceValueHolder.getCarbonInputEventAdapterService();
		EventManagementService eventManagementService = EventManagementServiceValueHolder.getCarbonEventManagementService();
		EventStreamService eventStreamService = EventStreamServiceValueHolder.getCarbonEventStreamService();
		
		EventReceiverServiceDS eventReceiverServiceDS = new EventReceiverServiceDS();
		eventReceiverServiceDS.setEventAdapterType(wso2EventEventAdapterFactory);
		eventReceiverServiceDS.setInputEventAdapterService(inputEventAdapterService);
		eventReceiverServiceDS.setEventManagementService(eventManagementService);
		eventReceiverServiceDS.setEventStreamService(eventStreamService);
		eventReceiverServiceDS.activate();
	}
	
	private void activateEventPublisherService() {
		RDBMSEventAdapterServiceDS rdbmsEventAdapterServiceDS = new RDBMSEventAdapterServiceDS();
		initSystemDataSources();
		//initTenantUserDataSources();
		DataSourceService dataSourceService = new DataSourceService();
		rdbmsEventAdapterServiceDS.setDataSourceService(dataSourceService);
		OutputEventAdapterFactory rdbmsEventAdapterFactory = new RDBMSEventAdapterFactory();
		
		OutputEventAdapterServiceDS outputEventAdapterServiceDS = new OutputEventAdapterServiceDS();
		outputEventAdapterServiceDS.setEventAdapterType(rdbmsEventAdapterFactory);
		outputEventAdapterServiceDS.activate();
		
		OutputEventAdapterService outputEventAdapterService = OutputEventAdapterServiceValueHolder.getCarbonOutputEventAdapterService();
		EventManagementService eventManagementService = EventManagementServiceValueHolder.getCarbonEventManagementService();
		EventStreamService eventStreamService = EventStreamServiceValueHolder.getCarbonEventStreamService();
		
		//eventStreamService.subscribe(eventProducer);
		
		EventPublisherServiceDS eventPublisherServiceDS = new EventPublisherServiceDS();
		eventPublisherServiceDS.setEventAdapterType(rdbmsEventAdapterFactory);
		eventPublisherServiceDS.setEventAdapterService(outputEventAdapterService);
		eventPublisherServiceDS.setEventManagementService(eventManagementService);
		eventPublisherServiceDS.setEventStreamService(eventStreamService);
		eventPublisherServiceDS.activate();
	}
	
	private synchronized void initTenantUserDataSources() {
		int tenantId = PrivilegedBarleyContext.getThreadLocalCarbonContext().getTenantId();
    	try {
    		if (log.isDebugEnabled()) {
        		log.debug("Initializing super tenant user data sources...");
        	}
    		DataSourceManager.getInstance().initTenant(tenantId);
    	    if (log.isDebugEnabled()) {
    	    	log.debug("tenant user data sources successfully initialized");
    	    }
    	} catch (Exception e) {
			log.error("Error in intializing system data sources: " + e.getMessage(), e);
		} 
    }
	
	private void initSystemDataSources() {
		try {
			DataSourceManager.getInstance().initSystemDataSources();
		} catch (DataSourceException e) {
			log.error("Error in intializing system data sources: " + e.getMessage(), e);
		};
	}


	public void start() {
		numberOfEventsReceived = new AtomicInteger(0);
		receiverComp.activate();
	}
	
	public void stop() {
		receiverComp.deactivate();
	}
	
	public int getNumberOfEventsReceived() {
        if (numberOfEventsReceived != null) return numberOfEventsReceived.get();
        else return 0;
    }

    public void resetReceivedEvents() {
        numberOfEventsReceived.set(0);
    }
    
    private static RealmService realmService = null;
    
    public static void init() {
    	initBarleyProperty();
		initKeyStore();
		//initTrustStore();
		initBaseService();
		initTenantDomain();
		initDataHolder();
	}
	
	private static void initBarleyProperty() {
		String rootPath = "D:\\Workspace_STS_SaaSPlatform\\Workspace_STS_Analytics\\barley.analytics-common\\data-bridge\\barley.databridge.agent";
		String configPath = rootPath + File.separator + "src\\test\\resources\\" + "repository" + File.separator + "conf";
		System.setProperty(ServerConstants.CARBON_HOME, rootPath);
        System.setProperty(ServerConstants.CARBON_CONFIG_DIR_PATH, configPath);
        System.setProperty("carbon.registry.character.encoding", "UTF-8");
        
        // The line below is responsible for initializing the cache.
        BarleyContextDataHolder.getCurrentCarbonContextHolder();
	}
	
	private static void initKeyStore() {
		String resourcesPath = BarleyUtils.getCarbonHome() + File.separator + "src\\test\\resources\\repository\\resources";
		System.setProperty("Security.KeyStore.Location", resourcesPath + File.separator + "security" + File.separator + "wso2carbon.jks");
		System.setProperty("Security.KeyStore.Password", "wso2carbon");
	}
	
	private static void initTrustStore() {
		String resourcesPath = BarleyUtils.getCarbonHome() + File.separator + "src\\test\\resources\\repository\\resources";
		System.setProperty("javax.net.ssl.trustStore", resourcesPath + File.separator + "security" + File.separator + "client-truststore.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "wso2carbon");
	}
	
	private static void initTenantDomain() {
//		String tenantDomain = "codefarm.co.kr";
//		int tenantId = 1;
		String tenantDomain = MultitenantConstants.SUPER_TENANT_DOMAIN;
		int tenantId = MultitenantConstants.SUPER_TENANT_ID;
		PrivilegedBarleyContext.getThreadLocalCarbonContext().setTenantDomain(tenantDomain, true);
    	PrivilegedBarleyContext.getThreadLocalCarbonContext().setTenantId(tenantId);
	}
	
	private static void initBaseService() {
		try {
        	BasicDataSource dataSource = new BasicDataSource();
        	String connectionUrl = "jdbc:mysql://172.16.2.201:3306/barley_registry";
            dataSource.setUrl(connectionUrl);
            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
            dataSource.setUsername("cdfcloud");
            dataSource.setPassword("cdfcloud");
        	
            realmService = new InMemoryRealmService(dataSource);
            InputStream is = new FileInputStream(BarleyUtils.getCarbonConfigDirPath() + File.separator + "registry.xml");
            // registry.xml 정보와 DataSource가 가미된 realmService를 인자로 주어 RegistryContext를 생성한다. 
            RegistryContext ctx = RegistryContext.getBaseInstance(is, realmService);
            
            ctx.setSetup(true);
            ctx.selectDBConfig("mysql-db");
            
            realmService = ctx.getRealmService();
        } catch (Exception e) {
        	e.printStackTrace();
        }
	}
	
	private static void initDataHolder() {
		OSGiDataHolder.getInstance().setUserRealmService(realmService);
		DataBridgeServiceValueHolder.setRealmService(realmService);
	}
    

	public static void main(String[] args) throws Exception {
    	init();
		BinaryTestServer server = new BinaryTestServer();
		server.setUp(realmService);
		server.start();
	}
	
}
